# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from posters.models import Poster, Genre

# Register your models here.
admin.site.register(Genre)
admin.site.register(Poster)
