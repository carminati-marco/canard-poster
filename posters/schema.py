# source: https://github.com/graphql-python/graphene-django/
# https://faceposter.github.io/relay/graphql/mutations.htm
# https://faceposter.github.io/relay/graphql/mutations.htm
# http://docs.graphene-python.org/projects/django/en/latest/tutorial-plain/

import graphene

from graphene import relay, ObjectType, InputObjectType
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from django.core.exceptions import ValidationError

from posters.models import Poster
from posters.helpers import get_object, get_errors, update_create_instance


class PosterInput(InputObjectType):
    """
    Class created to accept input data
    from the interactive graphql console.
    """

    imdb_id = graphene.Int()
    title = graphene.String(max_length=200, required=True)
    year = graphene.Int()
    imdb_score = graphene.Int()
    genre = graphene.String()
    poster_url = graphene.String()


class PosterNode(DjangoObjectType):
    class Meta:
        model = Poster
        filter_fields = {
            'title': ['exact', 'istartswith'],
            'imdb_id': ['exact'],
            'genre': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node, )


class CreatePoster(graphene.relay.ClientIDMutation):

    poster = graphene.Field(PosterNode)
    errors = graphene.List(graphene.String)

    class Input:
        poster = graphene.Argument(PosterInput)

    def mutate_and_get_payload(root, info, **input):
        poster = Poster()
        poster = update_create_instance(poster, input.get('poster'))
        return CreatePoster(poster=poster)


class UpdatePoster(relay.ClientIDMutation):

    class Input:
        poster = graphene.Argument(PosterInput)
        id = graphene.String(required=True)  # the poster id

    errors = graphene.List(graphene.String)
    poster = graphene.Field(PosterNode)

    def mutate_and_get_payload(root, info, **input):
        try:
            poster_instance = get_object(
                Poster, input.get('id'))  # get poster by id
            if poster_instance:
                poster = update_create_instance(
                    poster_instance, input.get('poster'))
                return UpdatePoster(poster=poster)
        except ValidationError as e:
            return UpdatePoster(poster=None, errors=get_errors(e))


class Query(ObjectType):
    poster = relay.Node.Field(PosterNode)
    posters = DjangoFilterConnectionField(PosterNode)

    def resolve_all_posters(self, info, **kwargs):
        return Poster.objects.all()


class Mutation(ObjectType):
    create_poster = CreatePoster.Field()
    update_poster = UpdatePoster.Field()


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
)
