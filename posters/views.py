# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, GenericAPIView
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from posters.models import Poster
from posters.serializers import PosterSerializer


class PosterListCreateView(ListCreateAPIView):
    model = Poster
    queryset = Poster.objects.all()
    serializer_class = PosterSerializer

    @permission_classes((IsAdminUser,))
    def create(self, request, *args, **kwargs):
        return super(PosterListCreateView, self).create(request, *args, **kwargs)

    @permission_classes((IsAuthenticated,))
    def list(self, request, *args, **kwargs):
        return super(PosterListCreateView, self).list(request, *args, **kwargs)


class PosterDetail(GenericAPIView):
    model = Poster
    serializer_class = PosterSerializer()
    queryset = Poster.objects.all()

    def get(self, request, pk):
        poster = self.get_object()
        serializer = PosterSerializer(poster, context={'request': request})
        return Response(serializer.data)
