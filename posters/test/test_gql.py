from django.test import TestCase
from graphene.test import Client
from mock import Mock
from graphql_relay import to_global_id, from_global_id
from posters.models import Poster
from posters.schema import schema


class TestPostersGraphQLSchema(TestCase):
    def setUp(self):
        self.poster = Poster.objects.create(imdb_id="1234", title="Toy Story", imdb_score=10,
                                            year=1995, genre="Animation",
                                            poster_url="https://lumiere-a.akamaihd.net/v1/images/c3c2b4a3323c4a71929cd5fc76bcda4df7157175.jpeg")

        self.client = Client(schema)
        self.context = Mock()

    def test_posters(self):

        result = self.client.execute("""
        query {
            posters {
                edges {
                    node {
                        title
                        year
                    }
                }
            }
        }
        """, context=self.context)

        posterGQL = result['data']['posters']['edges'][0]['node']
        self.assertEquals(posterGQL['title'], 'Toy Story')
        self.assertEquals(posterGQL['year'], 1995)

    def test_poster(self):

        result = self.client.execute("""
        query Q($poster_id: ID!){
                poster (id: $poster_id){
                  id
                  title
                  year
                }
            }
        """, variable_values={"poster_id": to_global_id('PosterNode', self.poster.id)},
            context=self.context)

        posterGQL = result['data']['poster']
        self.assertEquals(posterGQL['title'], 'Toy Story')
        self.assertEquals(posterGQL['year'], 1995)


class TestPosterGRaphQLMutation(TestCase):

    def setUp(self):
        self.client = Client(schema)
        self.context = Mock()

    def test_poster_create(self):
        result = self.client.execute(
            """
            mutation CreatePoster($input: CreatePosterInput!){
                createPoster(input: $input) {
                    poster {
                        id
                        title
                    }
                    clientMutationId
                }
            }

            """,
            variable_values={"input": {"clientMutationId": "abc",
                                       "poster": {"genre": "Animation", "title": "Toy Story"}}},
            context=self.context
        )
        poster = result['data']['createPoster']['poster']
        self.assertEquals(poster['title'], "Toy Story")
        self.assertEquals(result['data']['createPoster']
                          ['clientMutationId'], "abc")

    def test_update_poster_create(self):

        poster = Poster.objects.create(imdb_id="1234", title="Toy Story", imdb_score=10,
                                       year=1995, genre="Animation",
                                       poster_url="https://lumiere-a.akamaihd.net/v1/images/c3c2b4a3323c4a71929cd5fc76bcda4df7157175.jpeg")
        result = self.client.execute(
            """
            mutation UpdatePoster($input: UpdatePosterInput!){
                updatePoster(input: $input) {
                    poster {
                        id
                        title
                        imdbScore
                    }
                    clientMutationId
                }
            }

            """,
            variable_values={"input": {"clientMutationId": "ddd", "id": to_global_id('PosterNode', poster.id),
                                       "poster": {"title": "Toy Story 2", "imdbScore": 8}}},
            context=self.context
        )

        poster_gql = result['data']['updatePoster']['poster']
        self.assertEquals(poster_gql['title'], "Toy Story 2")
        self.assertEquals(poster_gql['imdbScore'], 8)
        self.assertEquals(from_global_id(
            poster_gql['id']), ('PosterNode', str(poster.id)))

        self.assertEquals(result['data']['updatePoster']
                          ['clientMutationId'], "ddd")
