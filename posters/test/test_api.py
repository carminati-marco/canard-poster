from django.urls import reverse
from rest_framework.test import APITestCase
from posters.models import Poster


class PosterAPITestCase(APITestCase):

    def setUp(self):
        self.poster = Poster.objects.create(imdb_id="1234", title="Toy Story", imdb_score=10,
                                            year=1995, genre="Animation",
                                            poster_url="https://lumiere-a.akamaihd.net/v1/images/c3c2b4a3323c4a71929cd5fc76bcda4df7157175.jpeg")

    def test_get_posters(self):
        response = self.client.get(reverse('posters'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [{'imdb_id': 1234, 'title': 'Toy Story', 'genre': 'Animation', 'imdb_score': '10.00',
                                            'poster_url': 'https://lumiere-a.akamaihd.net/v1/images/c3c2b4a3323c4a71929cd5fc76bcda4df7157175.jpeg'}])

    def test_get_poster_detail(self):
        response = self.client.get(
            reverse('poster-detail', kwargs={'pk': self.poster.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'imdb_id': 1234, 'title': 'Toy Story', 'genre': 'Animation', 'imdb_score': '10.00',
                                           'poster_url': 'https://lumiere-a.akamaihd.net/v1/images/c3c2b4a3323c4a71929cd5fc76bcda4df7157175.jpeg'})
