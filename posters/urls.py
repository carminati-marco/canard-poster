from django.conf.urls import url
from posters import views

urlpatterns = [
    url(r'posters/$', views.PosterListCreateView.as_view(), name='posters'),
    url(r'posters/detail/(?P<pk>[0-9]+)/$',
        views.PosterDetail.as_view(), name='poster-detail'),
]
