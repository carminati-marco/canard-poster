# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# Create your models here.
from django.db import models


class Genre(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Poster(models.Model):

    imdb_id = models.IntegerField(null=True, blank=True)
    title = models.CharField(max_length=200)
    year = models.IntegerField(null=True, blank=True)
    imdb_score = models.DecimalField(
        decimal_places=2, max_digits=6, null=True, blank=True)
    genre = models.CharField(max_length=200, null=True, blank=True)
    genres = models.ManyToManyField(Genre, blank=True)
    poster_url = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
