from os import path
import re
import pandas as pd
from django.core.management.base import BaseCommand
from django.db import transaction
from posters.models import Poster, Genre


class Command(BaseCommand):
    help = 'Init fixtures to create some posters'

    def add_arguments(self, parser):
        parser.add_argument('items', type=int,
                            help='Number of items you want to import',)

    def handle(self, *args, **options):

        items = options.get('items', 40109)
        df = pd.read_csv(path.join(path.dirname(__file__),
                                   "posters.csv"), encoding='latin1')
        with transaction.atomic():
            for _, row in df[:items].iterrows():
                print(f"Check {row['Title']}")
                m = re.search("(.+?) \((\d{4})\)", row['Title'])
                title, year = m.groups() if m is not None else (
                    row['Title'], None)

                poster, _ = Poster.objects.get_or_create(
                    imdb_id=row['imdbId'], title=title, year=year,
                    imdb_score=row['IMDB Score'], poster_url=row['Poster'])
                if type(row['Genre']) == str:
                    poster.genre = row['Genre']
                    for genre_name in row['Genre'].split('|'):
                        genre, _ = Genre.objects.get_or_create(name=genre_name)
                        poster.genres.add(genre)

                    poster.save()
                    print(poster.genres.all())
