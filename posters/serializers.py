from rest_framework import serializers

from posters.models import Poster


class PosterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Poster
        fields = ('imdb_id', 'title', 'genre', 'imdb_score', 'poster_url')
