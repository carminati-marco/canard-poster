import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='canard-posters',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='Another Django app to conduct Web-based posters.',
    long_description=README,
    url='https://www.example.com/',
    author='Your Name',
    author_email='yourname@example.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: X.Y',  # replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'Django~=2.1.5',
        'djangorestframework~=3.9.1',
        'graphene~=2.1.3',
        'graphene-django~=2.2.0',
        'graphql-core~=2.1',
        'graphql-relay~=0.4.5',
        'mock~=2.0.0',
        'pandas~=0.24.1']
)
