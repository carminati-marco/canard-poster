Django==2.1.5
djangorestframework==3.9.1
graphene==2.1.3
graphene-django==2.2.0
graphql-core==2.1
graphql-relay==0.4.5
mock==2.0.0
pandas==0.24.1
