=====
Posters
=====

Posters is a simple Django app
Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "posters" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'posters',
    ]

2. Include the posters URLconf in your project urls.py like this::

    path('posters/', include('posters.urls')),

3. Include the posters schema in your project settigns.py like this::

    GRAPHENE = {
        'SCHEMA': 'posters.schema.schema',
        'MIDDLEWARE': (
            'graphene_django.debug.DjangoDebugMiddleware',
        )
    }


4. Run `python manage.py migrate` to create the posters models.

5. Run `python manage.py init_fixtures_posters <num>` replacing <num> with number of items you want create (max 40109)`  
